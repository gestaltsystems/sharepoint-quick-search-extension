/*

	Search As You Type for SharePoint 2010
	Based on Jan Tielens' <http://weblogs.asp.net/jan>
	Licence: GPL v3
	Copright: Gestalt Systems Ltd <http://www.gestaltsystems.co.uk/>
	Current Version: 1.3 
	
	The JavaScript code in this page is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License (GNU GPL) as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.  The code is distributed WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

    As additional permission under GNU GPL version 3 section 7, you
    may distribute non-source (e.g., minimized or compacted) forms of
    that code without the copy of the GNU GPL normally required by
    section 4, provided you include this license notice and a URL
    through which recipients can access the Corresponding Source.
	

*/
String.prototype.trim=function(){return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');};  

var quickSearchConfig = {
        delay: 500,             // time to wait before executing the query (in ms)
        minCharacters: 3,       // minimum nr of characters to enter before search
        scope: "All Sites",     // search scope to use
        numberOfResults: 15,    // number of results to show
        resultsAnimation: 200,  // animation time (in ms) of the search results
        resultAnimation: 0      // animation time (in ms) of individual result (when selected)
    };    

var debug ="";
var PeopleSearch = false;
var quickSearchTimer;
var quickSearchSelectedDivIndex = -1;
var inputField = null;
var defPhoto = "/_layouts/images/o14_person_placeholder_42.png";
var peopleSearchResultsUrl = "/path/to/Peopleresults.aspx?k="
var SearchGoButton = "#ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_go";
var peopleSearchResults = "path/to/peopleresults.aspx";
function showResultsDiv(text) {
        var div = $("#quickSearchResults");
        var prevTable = inputField;
        
       var divCss = {
            "left": prevTable.offset().left,
            "padding": 2,
            "position": "absolute",
	    "top": prevTable.offset().top-15,
            "border": "1px solid #7f9db9",
            "width": prevTable.width() + 300,
            "background": "white",
            "max-width": 300,
	    "z-index": 1500
            };


                  
        div.css(divCss).append(text).slideDown(quickSearchConfig.resultsAnimation);
    }
    
$(document).ready(function(){
    
    inputField= $("#ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords");
    if($(inputField).length > 0){
	if($(inputField).val().indexOf("*")>-1){
	    $(inputField).val($(inputField).val().replace(/\*/g," ").trim());
	}
    }
   
    
   
    if(window.location.pathname.toLowerCase().indexOf(peopleSearchResults.toLowerCase())>-1){
	$("#vwgTogglePeopleSearch").addClass("vwgPSTOn");
	 PeopleSearch = true;
	
	$(SearchGoButton).click(function(){
	    window.location = peopleSearchResultsUrl + encodeURI($(inputField).val().replace(" ","*"))+"*";
	    return false;
	});
	
    }
    $("#vwgTogglePeopleSearch").click(function(){
	$(this).toggleClass("vwgPSTOn");
	if(PeopleSearch == true){
	    PeopleSearch = false;
	    $(SearchGoButton).unbind("click");
	}
	else{
	    PeopleSearch = true;
	    $(SearchGoButton).click(function(){
		window.location = peopleSearchResultsUrl + encodeURI($(inputField).val().replace(" ","*"))+"*";
		return false;
	    });
	}
	$(inputField).focus();
    });

    $(inputField).keyup(function(event) {
        var previousSelected = quickSearchSelectedDivIndex;
            
            // catch some keys
        switch(event.keyCode) {
        case 13:    // enter
            var selectedDiv = $("#quickSearchResults>div:eq(" + quickSearchSelectedDivIndex + ") a");
            if(selectedDiv.length == 1)
                window.location = selectedDiv.attr("href");
	    else if (PeopleSearch == true)
		window.location = peopleSearchResultsUrl + encodeURI($(inputField).val().replace(" ","*"))+"*";
            break;
        case 38:    // key up
            quickSearchSelectedDivIndex--;
            break;
        case 40:    // key down
            quickSearchSelectedDivIndex ++;
            break;
	case 27:
	    $("#quickSearchResults").hide();
	    break;
        }
            
        // check bounds
        if(quickSearchSelectedDivIndex != previousSelected) {
            if(quickSearchSelectedDivIndex < 0)
                quickSearchSelectedDivIndex = 0;
            if(quickSearchSelectedDivIndex >= $("#quickSearchResults>div").length -1)
                quickSearchSelectedDivIndex = $("#quickSearchResults>div").length - 2;         
	}
            
            // select new div, unselect the previous selected
            if(quickSearchSelectedDivIndex > -1) {
                if(quickSearchSelectedDivIndex != previousSelected) {
                    unSelectDiv( $("#quickSearchResults>div:eq(" + previousSelected + ")"));
                    selectDiv($("#quickSearchResults>div:eq(" + quickSearchSelectedDivIndex + ")"));
                }
            }
            
            // if the query is different from the previous one, search again
            if($(inputField).data("query") != $(inputField).val()) {
                if (quickSearchTimer != null) // cancel the delayed event
                    clearTimeout(quickSearchTimer);
                quickSearchTimer = setTimeout(function() { // delay the searching
                        $("#quickSearchResults").fadeOut(200, initSearch);
                    } , quickSearchConfig.delay);
            }
        }); 
});

    function unSelectDiv(div) {
        // first stop all animations still in progress
        $("#quickSearchResults>div>div").stop(true,true);
        
        

div.removeClass("quickSearchResultDivSelected").addClass("quickSearchResultDivUnselected");  
        $("#details", div).hide();
    }
    
    function selectDiv(div) {
        div.addClass("quickSearchResultDivSelected");
        $("#details", div).slideDown(quickSearchConfig.resultAnimation);
    }
    
    function initSearch() {
        // first store query in data
        $(inputField).data("query", $(inputField).val());
        
        // clear the results
        $("#quickSearchResults").empty();
        
        // start the search
        var query = $(inputField).val();
        if(query.length >= quickSearchConfig.minCharacters) {
            showResultsDiv("Searching ..."); // display status
            search(query);
        }
    }
    
    function search(query) {
        quickSearchSelectedDivIndex = -1;
		if(PeopleSearch == true){
		  var queryXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\
							<QueryPacket xmlns=\"urn:Microsoft.Search.Query\" Revision=\"1000\">\
							<Query domain=\"QDomain\">\
							<SupportedFormats>\
							<Format>urn:Microsoft.Search.Response.Document.Document</Format>\
							</SupportedFormats>\
							<Context>\
							<QueryText language=\"en-US\" type=\"MSSQLFT\">\
							SELECT preferredname, Department, WorkPhone, Extension, WorkEmail, PictureURL, Path\
							FROM SCOPE() \
							where \"scope\"='People' AND \"preferredname\" LIKE '%" + query + "%'\
							</QueryText>\
							</Context>\
							</Query>\
							</QueryPacket>";
							
			var soapEnv = "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\
							<soap:Body>\
							<QueryEx xmlns='http://microsoft.com/webservices/OfficeServer/QueryService'>\
							<queryXml>" + escapeHTML(queryXML) + "</queryXml>\
							</QueryEx>\
							</soap:Body>\
							</soap:Envelope>";
		}
		else
		{
        var queryXML = "<QueryPacket xmlns='urn:Microsoft.Search.Query' Revision='1000'> \
							<Query domain='QDomain'> \
							<SupportedFormats><Format>urn:Microsoft.Search.Response.Document.Document</Format></SupportedFormats> \
							<Context> \
							<QueryText language='en-US' type='STRING' >SCOPE:\"" + quickSearchConfig.scope + "\"" + query + "</QueryText> \
							</Context> \
							<SortByProperties><SortByProperty name='Rank' direction='Descending' order='1'/></SortByProperties> \
							<Range><StartAt>1</StartAt><Count>" + quickSearchConfig.numberOfResults + "</Count></Range> \
							<EnableStemming>false</EnableStemming> \
							<TrimDuplicates>true</TrimDuplicates> \
							<IgnoreAllNoiseQuery>true</IgnoreAllNoiseQuery> \
							<ImplicitAndBehavior>true</ImplicitAndBehavior> \
							<IncludeRelevanceResults>true</IncludeRelevanceResults> \
							<IncludeSpecialTermResults>true</IncludeSpecialTermResults> \
							<IncludeHighConfidenceResults>true</IncludeHighConfidenceResults> \
							</Query></QueryPacket>";
							
        var soapEnv =   "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'> \
              <soap:Body> \
                <Query xmlns='urn:Microsoft.Search'> \
                  <queryXml>" + escapeHTML(queryXML) + "</queryXml> \
                </Query> \
              </soap:Body> \
            </soap:Envelope>";
		}
        $.ajax({
            url: "/_vti_bin/search.asmx",
            type: "POST",
            dataType: "xml",
            data: soapEnv,
            complete: processResult,
            contentType: "text/xml; charset=\"utf-8\""
        });
     }   
        function processResult(xData, status) {
			
            var html = "";
			if(PeopleSearch == true){
				var xmlDoc_ps = xData.responseXML;
				
				var docs_ps = xmlDoc_ps.selectNodes("//RelevantResults");
				var TotalResults_ps = docs_ps.length;
           
				var ResultsLimit_ps = 5;
				if (TotalResults_ps < 5){
					ResultsLimit_ps = TotalResults_ps;
				}
			
			    for(var i = 0; i < ResultsLimit_ps ; i++){
				
				var photo = defPhoto;
				if(docs_ps[i].selectSingleNode("PICTUREURL") !== null){
				    photo = docs_ps[i].selectSingleNode("PICTUREURL").text;
				}
				
				html += "<img src='"+ photo +"' class='vwgSPPhoto'/>";        
				html += "<div class='vwgSRDetails'>";
				
				if(docs_ps[i].selectSingleNode("PREFERREDNAME") !== null)
				    html += "<div><strong>Name:</strong> <a href='"+ docs_ps[i].selectSingleNode("PATH").text+"'>" + docs_ps[i].selectSingleNode("PREFERREDNAME").text + "</a></div>";
				if(docs_ps[i].selectSingleNode("DEPARTMENT") !== null)
				    html += "<div><strong>Department:</strong> "+ docs_ps[i].selectSingleNode("DEPARTMENT").text + "</div>";
				if(docs_ps[i].selectSingleNode("WORKEMAIL") !== null){
				    email = docs_ps[i].selectSingleNode("WORKEMAIL").text;
				    html += "<div><strong>Work E-mail:</strong> <a href='mailto:"+email +"'>"+email +"</a></div>";
				}
				if(docs_ps[i].selectSingleNode("WORKPHONE") !== null)
				    html += "<div><strong>Work Telephone:</strong> "+ docs_ps[i].selectSingleNode("WORKPHONE").text + "</div>";
				if(docs_ps[i].selectSingleNode("EXTENSION") !== null)
				    html += "<div><strong>Extension:</strong> "+ docs_ps[i].selectSingleNode("EXTENSION").text + "</div>";
				html +="</div>";           
				}
				
				html += "<div style='text-align:right'>Total results: "+ TotalResults_ps +"</div>";
			
			}
			else{
            $(xData.responseXML).find("QueryResult").each(function() {
                var divWidh = $("#quickSearchTable").width() - 13;
                
                var x = $("<xml>" + $(this).text() + "</xml>");
                x.find("Document").each(function() {
                    var title = $("Title", $(this)).text();
                    var url = $("Action>LinkUrl", $(this)).text();
                    var description = $("Description", $(this)).text()
                    
                    html += "<div class='quickSearchResultDivUnselected' style='width:" + divWidh + "px;max-width:" + divWidh +"px'> \
                            <a href='" + url + "'>" + $("Title", $(this)).text() + "</a> \
                            <div style='display:none' id='details' style='margin-left:10px'>" + description + "<br/>" + url + " \
                            </div> \
                        </div>";
                });
                if(x.find("TotalAvailable").text() != "")
                    html += "<div style='text-align:right'>Total results: " + x.find("TotalAvailable").text() + "</div>";
                else                        
                    html += "<div style='text-align:right'>Total results: 0</div>";
            });
            }
			
				
			
            $("#quickSearchResults").empty().append(html);
			$(".vwgSPPhoto").error(function(){
				$(this).attr("src", defPhoto); 
			}); 
            $("#quickSearchResults>div>a").hover(
                function() { selectDiv($(this).parent()); },
                function() { unSelectDiv($(this).parent());  }
            );                    
            showResultsDiv();
        }            
    
    
    function escapeHTML (str) {
       return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }
